const client = require('../pgClient.js');

/**
 * Fetches weapons(class) from the database
 */
async function getWeapons() {
  const resultObj = await client.query('select * from weapons');
  return resultObj.rows;
}

// exports all the weapons model functions
module.exports = {
  getWeapons
};
