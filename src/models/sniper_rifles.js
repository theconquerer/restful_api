const client = require('../pgClient.js');

/**
 * Fetches sniper rifle data from database.
 * @param string sniperRifleName Name of the sniper rifle that is required
 */
async function getSniperRifles(sniperRifleName) {
  let resultObj;
  if (sniperRifleName === null) {
    resultObj = await client.query('select * from sniper_rifles');
  } else {
    resultObj = await client.query(
      'select * from sniper_rifles where name = $1',
      [sniperRifleName]
    );
  }
  return resultObj.rows;
}

/**
 * Inserts/Adds a sniper rife to the database with the given values.
 * @param Array values An array containing values corresponding to fields in database
 */
async function postSniperRifle(values) {
  const queryString =
    'insert into sniper_rifles values($1,$2,$3,$4,$5,$6,$7) returning *';
  const resultObj = await client.query(queryString, values);
  return resultObj.rows[0];
}

/**
 * Updates the sniper rifle with the given values.
 * @param string sniperRifleName Name of the sniper rifle getting updated
 * @param Array values An array containing values corresponding to the fields in database
 */
async function putSniperRifle(sniperRifleName, values) {
  const queryString =
    'update sniper_rifles set (name,ammo_type,hit_damage,initial_bullet_speed,ammo_per_clip,time_between_shots_in_sec,reload_method) = ($1,$2,$3,$4,$5,$6,$7) where name = $8 returning *';
  const resultObj = await client.query(queryString, [
    ...values,
    sniperRifleName
  ]);
  return resultObj.rows[0];
}

/**
 * Deletes the sniper rifle with the given name.
 * @param string sniperRifleName Name of the sniper rifle getting deleted
 */
async function deleteSniperRifle(sniperRifleName) {
  const queryString = 'delete from sniper_rifles where name = $1 returning *';
  const resultObj = await client.query(queryString, [sniperRifleName]);
  return resultObj.rows[0];
}

// exports all the sniper rifle model functions
module.exports = {
  getSniperRifles,
  postSniperRifle,
  putSniperRifle,
  deleteSniperRifle
};
