const client = require('../pgClient.js');

/**
 * Fetches rifle data from database.
 * @param string rifleName Name of the rifle that is required
 */
async function getRifles(rifleName) {
  let resultObj;
  if (rifleName === null) {
    resultObj = await client.query('select * from rifles');
  } else {
    resultObj = await client.query('select * from rifles where name = $1', [
      rifleName
    ]);
  }
  return resultObj.rows;
}

/**
 * Inserts/Adds a rife to the database with the given values.
 * @param Array values An array containing values corresponding to fields in database
 */
async function postRifles(values) {
  const queryString =
    'insert into rifles values($1,$2,$3,$4,$5,$6,$7) returning *';
  const resultObj = await client.query(queryString, values);
  return resultObj.rows[0];
}

/**
 * Updates the rifle with the given values.
 * @param string rifleName Name of the rifle getting updated
 * @param Array values An array containing values corresponding to the fields in database
 */
async function putRifles(rifleName, values) {
  const queryString =
    'update rifles set (name,ammo_type,hit_damage,initial_bullet_speed,damage_per_second,ammo_per_clip,time_between_shots_in_sec) = ($1,$2,$3,$4,$5,$6,$7) where name = $8 returning *';
  const resultObj = await client.query(queryString, [...values, rifleName]);
  return resultObj.rows[0];
}

/**
 * Deletes the rifle with the given name.
 * @param string rifleName Name of the rifle getting deleted
 */
async function deleteRifle(rifleName) {
  const queryString = 'delete from rifles where name = $1 returning *';
  const resultObj = await client.query(queryString, [rifleName]);
  return resultObj.rows[0];
}

// exports all the rifle model functions
module.exports = {
  getRifles,
  postRifles,
  putRifles,
  deleteRifle
};
