const {
  getRifles,
  postRifles,
  putRifles,
  deleteRifle
} = require('../models/rifles.js');

/**
 * Gets rifles from the model and
 * sends the fetched data in response.
 */
function getRiflesRouteHandler(req, res) {
  getRifles(null).then(rifles => {
    res.send(rifles);
  });
}

/**
 * Gets individual rifle from the model
 * and sends the fetched data in response.
 */
function getRifleByNameRouteHandler(req, res) {
  const rifleName = req.params.name;
  getRifles(rifleName).then(rifle => {
    res.send(rifle);
  });
}

/**
 * Adds the rifle to the postgreDB and
 * sends added gun in response.
 */
function postRiflesRouteHandler(req, res) {
  const values = Object.values(req.body);
  postRifles(values).then(rifle => {
    res.send(rifle);
  });
}

/**
 * Updates the rifle's details in the postgreDB
 * and sends the updated gun details in response.
 */
function putRifleByNameRouteHandler(req, res) {
  const rifleName = req.params.name;
  const values = Object.values(req.body);
  putRifles(rifleName, values).then(rifle => {
    res.send(rifle);
  });
}

/**
 * Deletes the rifle from the postgreDB and
 * sends the deleted gun details in response.
 */
function deleteRifleByNameRouteHandler(req, res) {
  const rifleName = req.params.name;
  deleteRifle(rifleName).then(rifle => {
    res.send(rifle);
  });
}


// exports all the rifles middleware function
module.exports = {
  getRiflesRouteHandler,
  getRifleByNameRouteHandler,
  postRiflesRouteHandler,
  putRifleByNameRouteHandler,
  deleteRifleByNameRouteHandler
};
