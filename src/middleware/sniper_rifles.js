const {
  getSniperRifles,
  postSniperRifle,
  putSniperRifle,
  deleteSniperRifle
} = require('../models/sniper_rifles.js');

/**
 * Gets sniper rifles from the model and
 * sends the fetched data in response.
 */
function getSniperRiflesRouteHandler(req, res) {
  getSniperRifles(null).then(sniperRifles => {
    res.send(sniperRifles);
  });
}

/**
 * Gets individual sniper rifle from the model
 * and sends the fetched data in response.
 */
function getSniperRifleByNameRouteHandler(req, res) {
  const rifleName = req.params.name;
  getSniperRifles(rifleName).then(sniperRifle => {
    res.send(sniperRifle);
  });
}

/**
 * Adds the sniper rifle to the postgreDB and
 * sends added gun in response.
 */
function postSniperRifleRouteHandler(req, res) {
  const values = Object.values(req.body);
  postSniperRifle(values).then(sniperRifle => {
    res.send(sniperRifle);
  });
}

/**
 * Updates the sniper rifle's details in the postgreDB
 * and sends the updated gun details in response.
 */
function putSniperRifleByNameRouteHandler(req, res) {
  const sniperRifleName = req.params.name;
  const values = Object.values(req.body);
  putSniperRifle(sniperRifleName, values).then(sniperRifle => {
    res.send(sniperRifle);
  });
}
/**
 * Deletes the sniper rifle from the postgreDB and
 * sends the deleted gun details in response.
 */
function deleteSniperRifleByNameRouteHandler(req, res) {
  const sniperRifleName = req.params.name;
  deleteSniperRifle(sniperRifleName).then(sniperRifle => {
    res.send(sniperRifle);
  });
}

// exports all the sniper rifles middleware function
module.exports = {
  getSniperRiflesRouteHandler,
  getSniperRifleByNameRouteHandler,
  postSniperRifleRouteHandler,
  putSniperRifleByNameRouteHandler,
  deleteSniperRifleByNameRouteHandler
};
