const express = require('express');
const { getWeaponsRouteHandler } = require('../middleware/weapons');
const riflesRouter = require('./rifles.js');
const sniperRiflesRouter = require('./sniper_rifles.js');

// creates a router instance
const weaponsRouter = express.Router();

// requests and endpoints helping map the appropriate middleware function or deeper routes

weaponsRouter.all('/', getWeaponsRouteHandler);
weaponsRouter.use('/rifles/', riflesRouter);
weaponsRouter.use('/sniper_rifles/', sniperRiflesRouter);

module.exports = weaponsRouter;
