const express = require('express');

// imports all the sniper rifles middleware functions
const {
  getSniperRiflesRouteHandler,
  getSniperRifleByNameRouteHandler,
  postSniperRifleRouteHandler,
  deleteSniperRifleByNameRouteHandler,
  putSniperRifleByNameRouteHandler
} = require('../middleware/sniper_rifles.js');

// creates a router instance
const sniperRiflesRouter = express.Router();

// uses middleware to create a json object of the req body
sniperRiflesRouter.use(express.json());


// requests and endpoints helping map the appropriate middleware function or deeper routes
sniperRiflesRouter.get('/', getSniperRiflesRouteHandler);
sniperRiflesRouter.get('/:name', getSniperRifleByNameRouteHandler);
sniperRiflesRouter.post('/', postSniperRifleRouteHandler);
sniperRiflesRouter.put('/:name', putSniperRifleByNameRouteHandler);
sniperRiflesRouter.delete('/:name', deleteSniperRifleByNameRouteHandler);

module.exports = sniperRiflesRouter;
