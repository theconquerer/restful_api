const { Client } = require('pg');

const connectString =
  'postgresql://postgres:test123@localhost:5432/weapons_database';
const client = new Client({
  connectionString: connectString,
});

module.exports = client;
