const client = require('../pgClient.js');
/**
 * Drops the given table if it exists in DB and then, creates the table with
 * data from the CSV filePath.
 *
 * @param string tableName - Name of the table
 * @param string fields - Fields and datatype string of CREATE TABLE
 * @param string filePath - Path of the CSV File
 */
async function createTable(tableName, fields, filePath) {
  console.log(filePath);
  try {
    await client.query(`DROP TABLE IF EXISTS ${tableName} CASCADE`);
    await client.query(`CREATE TABLE ${tableName}(${fields})`);
    await client.query(
      `COPY ${tableName} FROM '${filePath}' DELIMITER ',' CSV HEADER`
    );
  } catch (e) {
    console.log('SQL Exception');
    console.log(e);
  }
}
async function seed() {
  await client.connect();
  const dirPath = __dirname;
  const path = dirPath.substring(
    0,
    dirPath.lastIndexOf('/', dirPath.lastIndexOf('/') - 1)
  );
  await createTable('weapons', 'name VARCHAR', `${path}/weapons.csv`);
  await createTable(
    'rifles',
    'name VARCHAR,ammo_type VARCHAR,hit_damage INT,initial_bullet_speed INT,damage_per_second INT,ammo_per_clip INT,time_between_shots_in_sec NUMERIC',
    `${path}/rifles.csv`
  );
  await createTable(
    'sniper_rifles',
    'name VARCHAR,ammo_type VARCHAR,hit_damage INT,initial_bullet_speed INT,ammo_per_clip INT,time_between_shots_in_sec NUMERIC,reload_method VARCHAR',
    `${path}/sniper_rifles.csv`
  );
  await client.end();
}
seed();
