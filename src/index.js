const express = require('express');
const weaponsRouter = require('./routes/weapons.js');

const client = require('./pgClient.js');

//creating an express application
const app = express();
//connecting to postgres DB
client.connect();

//app now listens for requests at PORT 5000
app.listen(5000, () => {
  console.log('Listening at port 5000');
});
//uses weaponsRouter when endpoint gets a hit
app.use('/api/weapons/', weaponsRouter);
